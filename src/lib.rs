use rand::Rng;

pub fn roll_dice(faces: usize) -> usize {
    rand::thread_rng().gen_range(1..=faces)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_roll_dice() {
        let result = roll_dice(6);
        assert!(result > 0 && result <= 6)
    }
}
